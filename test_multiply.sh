#!/bin/sh

# Happy test, nothing wrong here!!

a=2
b=2
val=`expr $a \* $b`

if [ "$val" -eq 4 ]
then
  echo "Ta-da! Multiplication is done correctly!"
else
  echo "Uh-oh ... Something is wrong!"
  exit 1
fi;
